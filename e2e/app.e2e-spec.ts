import { OuctionPage } from './app.po';

describe('ouction App', () => {
  let page: OuctionPage;

  beforeEach(() => {
    page = new OuctionPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
